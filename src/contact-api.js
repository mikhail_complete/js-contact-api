/**
 * Created by rusakovich on 25.03.15.
 */
function ContactAPI(options) {

    var SUCCESS_STATUS = 200,

        defaultOptions = {
            //EDS Proxy local url
            url: document.location.protocol == "https:" ? "https://localhost:7756" : "http://localhost:7755",
            //EDS Proxy methods
            methods: {
                signString: "signstring",
                verifyString: "verifystring"
            },

            messages: {
                connectionError: "EDS Proxy connection error",
                corsNotSupported: "CORS not supported"
            },

            showMessage: function (message) {
                window.alert(message);
            }

        };

    for (var option in defaultOptions) {
        this[option] = options && options[option] !== undefined
            ? options[option] : defaultOptions[option];
    }

    function createXHR(method, url) {
        var xhr = new XMLHttpRequest();

        if ("withCredentials" in xhr) {
            // XHR for Chrome/Firefox/Opera/Safari/IE10.
            xhr.open(method, url, true);
        } else if (typeof XDomainRequest != "undefined") {
            // XDomainRequest for IE.
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            // CORS not supported.
            var _message = this.messages.corsNotSupported;
            this.showMessage(_message);
            xhr = null;
        }

        return xhr;
    }

    function setHeader(xhr) {
        if (xhr.setRequestHeader != "undefined") {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
        } else {
            xhr.contentType = "application/x-www-form-urlencoded; charset=UTF-8";
        }
    }

    function processRequest(xhr, successCallback, errorCallback) {
        var _this = this;

        if (xhr) {

            xhr.onload = function (result) {
                var responseText = xhr.responseText;
                if (xhr.status == SUCCESS_STATUS) {
                    successCallback(responseText);
                } else {
                    errorCallback(responseText);
                }
            };

            xhr.onerror = function (result) {
                if (xhr.responseText == "") {
                    var _message = _this.messages.connectionError
                    _this.showMessage(_message);
                    return;
                }

                errorCallback(xhr.responseText);
            };

            setHeader(xhr)
        }
    }


    /**
     * Contact sign method
     *
     * @param content - content to sign
     * @param successCallback - success callback to process response
     * @param errorCallback - error callback to process response, when error occur while signing
     */
    ContactAPI.prototype.sign = function (content, timestamp, successCallback, errorCallback) {
        var params = "string=" + encodeURIComponent(content) + "&timestamp=" + timestamp;

        var xhr = createXHR.call(this, 'POST', this.url + "/" + this.methods.signString);

        processRequest.call(this, xhr, successCallback, errorCallback);

        xhr.send(params);
    }

    /**
     * Contact verify method
     *
     * @param content - content to verify
     * @param signature - content accordable signature
     * @param successCallback - success callback to process response
     * @param errorCallback - error callback to process response, when error occur while verification
     */
    ContactAPI.prototype.verify = function (content, signature, successCallback, errorCallback) {
        var params = "string=" + encodeURIComponent(content) + "&signature=" + encodeURIComponent(signature);

        var xhr = createXHR.call(this, 'POST', this.url + "/" + this.methods.verifyString);

        processRequest.call(this, xhr, successCallback, errorCallback);

        xhr.send(params);
    }

}

