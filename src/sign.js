/**
 * Created by rusakovich on 25.03.15.
 */
var Sign = {
    provider: null,

    init: function () {
        Sign.provider = new ContactAPI({'url': 'http://localhost:7755'});
    },

    sign: function (string, successCallback, errorCallback) {


        var currentDate = (Date.now)? Date.now() : new Date().getTime(),
            timestamp = Math.floor(currentDate / 1000);

        Sign.provider.sign(string, timestamp,

            function (response) {
                var signature = $.parseJSON(response).signatureBase64;
                successCallback(signature);
            },

            function (response) {
                var errorMessage = $.parseJSON(response).message;
                errorCallback(errorMessage);
            }
        );
    },

    verify: function (string, signature, successCallback, errorCallback) {
        Sign.provider.verify(string, signature,

            function (response) {
                var deserialized = $.parseJSON(response),
                    serial = deserialized.serial,
                    issuer = deserialized.issuerInfo.commonName,
                    subject = deserialized.subjectInfo.commonName;

                successCallback(serial, issuer, subject);
            },

            function (response) {
                var errorMessage = $.parseJSON(response).message;
                errorCallback(errorMessage);
            }
        );
    }
}
