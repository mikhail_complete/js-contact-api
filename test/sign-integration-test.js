/**
 * Created by rusakovich on 25.03.15.
 */
var SignTest = AsyncTestCase('SignTest');

SignTest.prototype.testSign = function (queue) {
    jstestdriver.console.log("****** sign testing... *****");

    assertNotNull(Sign);

    Sign.init();

    var content = "test string";
    queue.call('test sign success', function (callbacks) {
            var signSuccessCallback = callbacks.add(function (signature) {
                assertNotNull(signature);
                assertNotNull(atob(signature));
            });

            Sign.sign(content,
                signSuccessCallback,
                function(){}
            )
        }
    );

    queue.call('test sign error', function (callbacks) {
            var signErrorCallback = callbacks.add(function (errorMessage) {
                jstestdriver.console.log(errorMessage);
                assertNotNull(errorMessage);
            });

            Sign.sign(content,
                function(){},
                signErrorCallback
            )
        }
    );

};

SignTest.prototype.testVerify = function (queue) {
    jstestdriver.console.log("****** verify testing... *****");

    assertNotNull(Sign);

    Sign.init();

    var content = samples.signedData;
    var validAvestSignature = samples.signature.avest;
    queue.call('test verify valid avest signature', function (callbacks) {
            var verifySuccessCallback = callbacks.add(function (serial, issuer, subject) {
                assertNotNull(serial);
                assertNotNull(issuer);
                assertNotNull(subject);

                jstestdriver.console.log(serial);
                jstestdriver.console.log(issuer);
                jstestdriver.console.log(subject);
            });

            Sign.verify(content,
                validAvestSignature,
                verifySuccessCallback,
                function(){}
            )

        }
    );

    var wrongContent = "testdfgdgf";
    queue.call('test wrong avest content', function (callbacks) {

            var verifyErrorCallback = callbacks.add(function (errorMessage) {
                jstestdriver.console.log(errorMessage);
                assertNotNull(errorMessage);
            });

            Sign.verify(wrongContent,
                validAvestSignature,
                function(){},
                verifyErrorCallback
            )

        }
    );


    var validContactSignature = samples.signature.contact;
    queue.call('test verify valid contact signature', function (callbacks) {
            var verifySuccessCallback = callbacks.add(function (serial, issuer, subject) {
                assertNotNull(serial);
                assertNotNull(issuer);
                assertNotNull(subject);

                jstestdriver.console.log(serial);
                jstestdriver.console.log(issuer);
                jstestdriver.console.log(subject);
            });

            Sign.verify(content,
                validContactSignature,
                verifySuccessCallback,
                function(){}
            )

        }
    );

    queue.call('test wrong contact content', function (callbacks) {

            var verifyErrorCallback = callbacks.add(function (errorMessage) {
                jstestdriver.console.log(errorMessage);
                assertNotNull(errorMessage);
            });

            Sign.verify(wrongContent,
                validContactSignature,
                function(){},
                verifyErrorCallback
            )

        }
    );




};